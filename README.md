# DataRose
*in-browser on-premise analytics*

- There's **no sign up** and **no install**
- For editing CSVs, JSON, XLSX files, we've integrated [js-xlsx](https://github.com/SheetJS/js-xlsx), and [sql.js](https://github.com/kripken/sql.js/) so you can query data entirely locally in your browser
- For connecting to **PostgreSQL**, **MySQL**, or **BigQuery**, just run a single command in your terminal to open a bridge that allows DataRose to directly connect to your database. Your data never touches a third party server.
- Chart with a single click
- Compare queries side by side
- The first version of this application is based on the React application franchise.sql written by Kevin Kwok and Guillermo Webster
    
# Running Locally

There's an online version of the DataRose app [right here](https://app.datarose.org). There are also instructions for building the DataRose app to static files [here](https://bitbucket.org/kloving/app/src/master/DEPLOYING_LOCALLY.md).

