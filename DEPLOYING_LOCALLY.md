If you want to try the DataRose app, there's an online version you try right now [right here](https://datarose.org/app).

Otherwise...

# Deploying Locally
0. **If you don't have `npm` or `yarn`, install** [yarn](https://yarnpkg.com/en/docs/install).

1. **Open up a terminal and run**

    ```bash
    git clone https://kloving@bitbucket.org/kloving/app.git
    ```

2. **cd into the project directory**
    ```bash
    cd app
    ```

3. **Install the project dependencies**
    ```bash
    yarn install
    ```

    (you can also run `npm install`)

4. **Build the DataRose app to static files**
    ```bash
    yarn build
    ```
    This command makes a folder named `/bundle` containing an `index.html` file which runs the DataRose app when you open it in a browser.

5. **Serve the static files**

    Use the http server of your choice to serve the contents of the `/bundle` directory. Using python, you might write:
    
    ```bash
    cd bundle
    python -m SimpleHTTPServer
    ```

6. (optional) **Email us at support@datarose.org if you're doing something interesting with the DataRose app!**
